const gulp = require("gulp");
const chalk = require('chalk');
const plumber = require('gulp-plumber');
const args = require('yargs').argv;
const runSequence = require('run-sequence');
const gulpif = require('gulp-if');
const htmlmin = require('gulp-htmlmin');
const data = require('gulp-data');

const pkg = require('./package.json');

// clean project
//******************************************
const del = require('del');

gulp.task('remove-build', (cb) => {
  return del('./build', cb);
});

// COPY
//******************************************
gulp.task('toRoot', () => {
  return gulp.src('./source/copyToRoot/**/*')
    .pipe(gulp.dest('./build'));
});

// Watch
//******************************************
const watch = require('gulp-watch');
gulp.task('watch', () => {
  watch('./source/copyToRoot/**/*', () => runSequence('toRoot'));
  watch('./source/pages/**/*', () => runSequence('html'));
  watch('./source/scss/**/*', () => runSequence('styles'));
});

// html
//******************************************
const nunjucksRender = require('gulp-nunjucks-render');
const moment = require('moment-timezone');
const time = moment().tz(pkg.clientTimeZone).format('DD MMM YYYY, HH:mm');

gulp.task('html', () => {
  gulp.src('./source/pages/*.nunj')
    .pipe(gulpif(args.dev, plumber({
      errorHandler: function(error) {
        console.log(chalk.red('ERROR: in HTML: ' + error.message));
      }
    })))
    .pipe(data({
      time: time,
      timeZone: pkg.clientTimeZone,
      project: pkg.title
    }))
    .pipe(nunjucksRender({
      path: './source/pages'
    }))
    .pipe(gulpif(args.build, htmlmin({ collapseWhitespace: true }) ))
    .pipe(gulp.dest('./build'));
});

// default task
//******************************************
gulp.task('default', () => {

  if (args.dev) {
    return runSequence(
      'remove-build',
      ['html'],
      'toRoot',
      'watch'
    );
  } else if (args.deploy) {
    return runSequence(
      'remove-build',
      ['html'],
      'toRoot',
      'sftp'
    );
  } else {
    return runSequence(
      'remove-build',
      'html',
      'toRoot'
    );
  }

});
